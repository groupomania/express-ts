FROM node:lts-alpine

WORKDIR /app

# === Install dependencies with frozen lockfile

COPY package.json .
COPY yarn.lock .
RUN yarn install --frozen-lockfile

# === Build the application

COPY . .
RUN yarn run db:generateclient
RUN yarn build

# === Metadata

EXPOSE 3000

CMD ["yarn", "start"]
