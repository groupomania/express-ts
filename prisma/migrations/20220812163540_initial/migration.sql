-- CreateTable
CREATE TABLE "User"
(
    "id"             UUID         NOT NULL,
    "name"           VARCHAR(32)  NOT NULL,
    "email"          VARCHAR(32)  NOT NULL,
    "password"       VARCHAR(256) NOT NULL,
    "role"           VARCHAR(32),
    "profilePicture" VARCHAR(64),
    "moderator"      BOOLEAN      NOT NULL DEFAULT false,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Post"
(
    "id"       UUID         NOT NULL,
    "datetime" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "authorId" UUID         NOT NULL,
    "content"  TEXT         NOT NULL,

    CONSTRAINT "Post_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserLikes"
(
    "userId" UUID NOT NULL,
    "postId" UUID NOT NULL,

    CONSTRAINT "UserLikes_pkey" PRIMARY KEY ("userId", "postId")
);

-- CreateTable
CREATE TABLE "Session"
(
    "id"        UUID         NOT NULL,
    "userId"    UUID         NOT NULL,
    "expiresOn" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Session_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User" ("email");

-- AddForeignKey
ALTER TABLE "Post"
    ADD CONSTRAINT "Post_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserLikes"
    ADD CONSTRAINT "UserLikes_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserLikes"
    ADD CONSTRAINT "UserLikes_postId_fkey" FOREIGN KEY ("postId") REFERENCES "Post" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Session"
    ADD CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;
