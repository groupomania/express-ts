-- AlterTable
ALTER TABLE "Post" ADD COLUMN     "commentToId" UUID;

-- AddForeignKey
ALTER TABLE "Post" ADD CONSTRAINT "Post_commentToId_fkey" FOREIGN KEY ("commentToId") REFERENCES "Post"("id") ON DELETE SET NULL ON UPDATE CASCADE;
