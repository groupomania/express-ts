# Groupomania Backend w/ Express (TS)

[TOC]

## Requirements

To run this project, you will need:

- [`yarn`](https://classic.yarnpkg.com/lang/en/docs/install)
- A running [PostgreSQL](https://www.postgresql.org/) DBMS with a dedicated database

## Setup

### Install dependencies

Make sure to install the dependencies:

```bash
yarn install
```

### Environment variables

Define the environment variables, relying on the `.env.template` file:

```bash
cp .env.template .env
```

Fill the placeholders with your own values.

#### Reference

| Variable       | Description                                                | Environment-dependent  |
|----------------|------------------------------------------------------------|:----------------------:|
| `DATABASE_URL` | DBMS credentials, formatted as a URL                       |           ✅            |
| `SECRET_KEY`   | JWT secret key, also used as a salt key to store passwords |           ✅            |

### Run database migrations

```bash
npx prisma migrate deploy
# or
yarn run db:migrate
```

## Deployment

### Development Server

Start the development server on http://localhost:3000

```bash
yarn run dev
```

### Production

Build the application for production:

```bash
yarn build
```

Locally preview production build:

```bash
yarn start
```

## Documentation

This API is self-documented following the OpenAPI 3 standards,
see http://localhost:3000/api-docs (development build only).

This documentation also serves as a validator for incoming requests,
outgoing responses and authorization.

### Generate static documentation

To generate a static documentation using redoc cli, you'll have to do:

```bash
# Install dependencies
yarn install

# Generate a "openapi.json" file at the project's root which contains the whole spec
yarn run docs:build

# Generate the static HTML documentation using redoc cli
yarn run docs:bundle
```

## Credits

- Based on the template [`express-generator-typescript`](https://github.com/seanpmaxwell/express-generator-typescript)
  © Sean Maxwell, [MIT License](https://choosealicense.com/licenses/mit/)
