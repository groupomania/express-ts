import { PrismaClient, Post, UserLikes } from "@prisma/client";

const prisma = new PrismaClient();

export type PostInclude = {
  author?: boolean,
  likes?: boolean,
  comments?: boolean,
};

export default {
  // async getAll(include: PostInclude = { author: true, likes: true }): Promise<Post[]> {
  //   return prisma.post.findMany({ include, orderBy: [{ datetime: "desc" }] });
  // },
  async getFilter(where: any, include: PostInclude = { author: true, likes: true, comments: true }): Promise<Post[]> {
    return prisma.post.findMany({ where, include, orderBy: [{ datetime: "desc" }] });
  },
  async getOne(where: any, include: PostInclude = { author: true, likes: true }): Promise<Post | null> {
    return prisma.post.findUnique({ where, include });
  },
  async create(content: string, authorId: string, commentToId?: string): Promise<Post | null> {
    // @ts-ignore
    const { id } = await prisma.post.create({ data: { authorId, commentToId, content } });
    return this.getOne({ id });
  },
  async update(where: any, newPost: any): Promise<Post | null> {
    await prisma.post.update({ where, data: { ...newPost } });
    return this.getOne(where);
  },
  async deleteOne(id: string): Promise<Post> {
    await prisma.userLikes.deleteMany({ where: { postId: id } });
    await prisma.post.deleteMany({ where: { commentToId: id } });
    return prisma.post.delete({ where: { id } });
  },
  async createLike(postId: string, userId: string): Promise<UserLikes> {
    return prisma.userLikes.create({ data: { postId, userId } });
  },
  async deleteLike(postId: string, userId: string): Promise<any> {
    return prisma.userLikes.deleteMany({ where: { postId, userId } });
  },
};
