import { PrismaClient, User } from "@prisma/client";
import crypto from "crypto";

const prisma = new PrismaClient();

type UpdatedProfile = {
  name?: string,
  email?: string,
  role?: string,
  profilePicture?: string,
  password?: string
}

export default {
  async getOne(where: any): Promise<User | null> {
    return prisma.user.findUnique({ where });
  },
  async create(name: string, email: string, pass: string, role?: string) {
    const hashedPassword = await this.hashPassword(pass);
    const user = await prisma.user.create({ data: { name, email, password: hashedPassword, role } });
    const { password, ...filteredUser } = user;
    return filteredUser;
  },
  async update(id: string, newData: UpdatedProfile) {
    return prisma.user.update({ where: { id }, data: { ...newData } });
  },
  async hashPassword(password: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const salt = crypto.randomBytes(16).toString("hex");
      crypto.scrypt(password, salt, 64, (err, derivedKey: Buffer) => {
        if (err) reject(err);
        resolve(`${salt}$$${derivedKey.toString("hex")}`);
      });
    });
  },
  async verifyPassword(password: string, hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const [salt, key] = hash.split("$$");
      crypto.scrypt(password, salt, 64, (err, derivedKey: Buffer) => {
        if (err) reject(err);
        resolve(key === derivedKey.toString("hex"));
      });
    });
  },
  async verifyCredentials(email: string, password: string): Promise<User | null> {
    const user = await this.getOne({ email });
    if (user && await this.verifyPassword(password, user.password)) {
      return user;
    } else {
      return null;
    }
  },
  async updatePassword(id: string, oldPassword: string, newPassword: string): Promise<boolean> {
    const user = await this.getOne({ id });
    if (user && await this.verifyPassword(oldPassword, user.password)) {
      await this.update(id, { password: await this.hashPassword(newPassword) });
      return true;
    } else {
      return false;
    }
  },
};