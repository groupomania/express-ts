import posts from "./posts/posts";
import users from "./users/users";

export default {
  posts,
  users,
};