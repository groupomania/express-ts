import jwt, { JwtPayload } from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import dotenv from "dotenv";
import { User } from "@prisma/client";

import users from "@models/users/users";

dotenv.config();

export default async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(" ")[1];
    const decodedToken: User = (<JwtPayload>await jwt.verify(String(token), process.env.SECRET_KEY || "s3cr3t")).data;
    req.user = <User>await users.getOne({ id: decodedToken.id });
    next();
  } catch (err) {
    res.status(401).send({ err, msg: "Invalid JWT" });
  }
}
