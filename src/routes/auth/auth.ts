import { Router, Request, Response } from "express";
import jwt from "jsonwebtoken";

import users from "@models/users/users";

const router = Router();

router.post("/login", async (req: Request, res: Response): Promise<void> => {
  const { email, password } = req.body;
  const user = await users.verifyCredentials(email, password);
  if (user) {
    const token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (7 * 864E5),
      data: { id: user.id, moderator: user.moderator },
    }, process.env.SECRET_KEY || "s3cr3t");
    res.status(200).send({ token });
  } else {
    res.sendStatus(401);
  }
});

router.post("/register", async (req: Request, res: Response): Promise<void> => {
  const { name, email, password, role } = req.body;
  res.status(201).send(await users.create(name, email, password, role));
});

export default router;
