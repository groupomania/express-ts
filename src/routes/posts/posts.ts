import { Router, Request, Response } from "express";

import auth from "@middleware/auth";
import posts from "@models/posts/posts";

const router = Router();

// === Posts

router.get("/", auth, async (req: Request, res: Response): Promise<void> => {
  res.status(200).send(await posts.getFilter({ commentToId: null }));
});

router.post("/", auth, async (req: Request, res: Response): Promise<void> => {
  const { content, authorId, commentToId } = req.body;
  if (req.user.id === authorId) {
    res.status(201).send(await posts.create(content, authorId, commentToId));
  } else {
    res.status(403).send({ msg: "Can't create post in place of another user." });
  }
});

router.get("/:id", auth, async (req: Request, res: Response): Promise<void> => {
  const { id } = req.params;
  const post = await posts.getOne({ id }, { author: true, likes: true, comments: true });
  res.status(post ? 200 : 404).send(post);
});

router.put("/:id", auth, async (req: Request, res: Response): Promise<void> => {
  const { id } = req.params;
  const post = await posts.getOne({ id });
  if (req.user.id === post?.authorId) {
    res.status(200).send(await posts.update({ id }, { ...req.body }));
  } else {
    res.sendStatus(403);
  }
});

router.delete("/:id", auth, async (req: Request, res: Response): Promise<void> => {
  const post = await posts.getOne({ id: req.params.id });
  if (req.user.id === post?.authorId || req.user.moderator) {
    res.status(200).send(await posts.deleteOne(req.params.id));
  } else {
    res.sendStatus(403);
  }
});

// === Likes

router.post("/:postId/likes", auth, async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.body;
  const { postId } = req.params;
  if (req.user.id === userId) {
    res.status(201).send(await posts.createLike(postId, userId));
  } else {
    res.sendStatus(403);
  }
});

router.delete("/:postId/likes/:userId", auth, async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;
  const { postId } = req.body;
  if (req.user.id === userId) {
    res.status(200).send(await posts.deleteLike(postId, userId));
  } else {
    res.sendStatus(403);
  }
});


// === Comments

router.get("/:postId/comments", auth, async (req: Request, res: Response): Promise<void> => {
  const { postId } = req.params;
  res.status(200).send(await posts.getFilter({ commentToId: postId }, { author: true, likes: true }));
});

export default router;
