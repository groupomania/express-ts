import { Router } from "express";

import postsRouter from "./posts/posts";
import authRouter from "./auth/auth";
import usersRouter from "./users/users";

// Export the base-router
const baseRouter = Router();

// Setup routers
baseRouter.use("/posts", postsRouter);
baseRouter.use("/auth", authRouter);
baseRouter.use("/users", usersRouter);

// Export default.
export default baseRouter;
