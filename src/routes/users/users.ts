import { Router, Request, Response, Express } from "express";

import users from "@models/users/users";
import auth from "@middleware/auth";

const router = Router();

router.get("/:id", auth, async (req: Request, res: Response): Promise<void> => {
  const { id } = req.params;
  if (req.user.id === id) {
    res.status(200).send(await users.getOne({ id }));
  } else {
    res.sendStatus(403);
  }
});

router.put("/:id", auth, async (req: Request, res: Response): Promise<void> => {
  if (req.user.id === req.params.id) {
    res.status(200).send(await users.update(req.params.id, { ...req.body }));
  } else {
    res.sendStatus(403);
  }
});

router.post("/:id/pp", auth, async (req: Request, res: Response) => {
  const files = <Express.Multer.File[] | undefined>req.files;
  if (files && files[0]) {
    if (req.user.id === req.params.id) {
      await users.update(req.params.id, { profilePicture: files[0].filename });
      res.sendStatus(201);
    } else {
      res.sendStatus(403);
    }
  } else {
    res.sendStatus(400);
  }
});

router.post("/:id/password", auth, async (req: Request, res: Response) => {
  const { oldPassword, newPassword } = req.body;
  if (req.user.id === req.params.id && await users.updatePassword(req.params.id, oldPassword, newPassword)) {
    res.sendStatus(201);
  } else {
    res.sendStatus(403);
  }
});

export default router;
