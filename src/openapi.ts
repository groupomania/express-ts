import fs from "fs";
import path from "path";
import yaml from "js-yaml";
import type { OpenAPIV3 } from "express-openapi-validator/dist/framework/types";

let pathsSpec: any = {};
let modelsSpec: any = {};

function loadDocs(rootPath: any, spec: any) {
  const dir = fs.readdirSync(rootPath, { withFileTypes: true });
  for (const file of dir) {
    if (file.isDirectory()) {
      loadDocs(path.join(rootPath, file.name), spec);
    } else {
      if ([".yml", ".yaml"].includes(path.extname(file.name))) {
        Object.assign(spec, yaml.load(fs.readFileSync(path.join(rootPath, file.name), "utf-8")));
      }
    }
  }
}

loadDocs(path.join(process.cwd(), "src", "routes"), pathsSpec);
loadDocs(path.join(process.cwd(), "src", "models"), modelsSpec);

export const openAPISpec: OpenAPIV3.Document = {
  openapi: "3.0.0",
  info: {
    title: "Groupomania API (express-ts)",
    contact: { name: "Sakiut", email: "contact@sakiut.fr" },
    license: {
      name: "European Union Public License, Version 1.2",
      url: "https://choosealicense.com/licenses/eupl-1.2/",
    },
    version: String(process.env.npm_package_version),
  },
  tags: [
    { name: "Authentication", description: "Endpoints used to authenticate the user" },
    { name: "Posts", description: "Posts-relative endpoints" },
    { name: "Users", description: "Endpoints related to the users' data" },
    { name: "Monitoring", description: "Endpoints related the application's health" },
  ],
  components: {
    schemas: modelsSpec,
    securitySchemes: { bearerAuth: { type: "http", scheme: "bearer", bearerFormat: "JWT" } },
  },
  paths: {
    "/status": {
      get: {
        tags: ["Monitoring"],
        summary: "Check if the backend is alive",
        responses: { 200: { description: "The backend is alive!" } },
      },
    },
    ...pathsSpec,
  },
};

if (!module.parent) {
  const file = fs.openSync(path.join(process.cwd(), "openapi.json"), "w");
  fs.writeFileSync(file, JSON.stringify(openAPISpec));
}
