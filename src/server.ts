import cookieParser from "cookie-parser";
import morgan from "morgan";
import path from "path";
import helmet from "helmet";
import cors from "cors";

import express, { Request, Response, NextFunction } from "express";
import "express-async-errors";
import swaggerUi from "swagger-ui-express";
import * as OpenApiValidator from "express-openapi-validator";

import { openAPISpec } from "./openapi";

import apiRouter from "./routes/api";

// Constants
const app = express();


/***********************************************************************************
 *                                  Middlewares
 **********************************************************************************/

// Common middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(cookieParser());

// Show routes called in console during development
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Security (helmet recommended in express docs)
if (process.env.NODE_ENV === "production") {
  app.use(helmet());
}

/***********************************************************************************
 *                         Documentation and validation
 **********************************************************************************/

if (process.env.NODE_ENV === "development") {
  app.get("/openapi.json", (req, res) => res.send(openAPISpec));
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(openAPISpec, {
    customCss: ".swagger-ui .topbar { display: none }",
  }));
}

app.use(OpenApiValidator.middleware({
  apiSpec: openAPISpec,
  validateRequests: true,
  validateResponses: true,
  validateSecurity: true,
  ignorePaths: (path: string) => path.startsWith("/public"),
  fileUploader: { dest: path.join(process.cwd(), "src", "public") },
}));

/***********************************************************************************
 *                         API routes and error handling
 **********************************************************************************/

app.get("/status", (req: Request, res: Response) => res.sendStatus(200));

// Add api router
app.use("/api", apiRouter);

// Error handling
app.use((err: any, _: Request, res: Response, __: NextFunction) => {
  console.error(err);
  res.status(err.status ? err.status : 500).send({ ...err });
});


/***********************************************************************************
 *                                  Front-end content
 **********************************************************************************/

// Set static dir
const staticDir = path.join(process.cwd(), "src", "public");
app.use("/public", express.static(staticDir));

// Export here and start in a diff file (for testing).
export default app;
